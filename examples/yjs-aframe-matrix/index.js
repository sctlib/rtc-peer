import mwc from "@sctlib/mwc";
import * as Y from "https://esm.sh/yjs";

const MATRIX_ROOM_ID = "!RHiUSYwUiwmpdlFazQ:matrix.org"

export default class RTCScene extends HTMLElement {
	get config() {
		const urlHashParams = new URLSearchParams(window.location.hash.slice(1));
		this.userId = urlHashParams.get("name") || urlHashParams.get("userId") || this.ydoc.guid;
		this.roomId = urlHashParams.get("roomId") || MATRIX_ROOM_ID;
		this.userColor = urlHashParams.get("color") || "green";
		this.eventType = "io.gitlab.sctlib.rtc.examples.yjs-aframe-matrix";
		this.stateKey = "";
		return {
			userId: this.userId,
			userColor: this.userColor,
			roomId: this.roomId,
			eventType: this.eventType,
			stateKey: this.stateKey,
		};
	}

	constructor() {
		super();

		const template = this.querySelector('template');
		if (!template) {
			throw new Error('Template not found in the document.');
		}
		const content = template.content.cloneNode(true);

		// Initialize core elements
		this.rtcUser = content.querySelector("rtc-user");
		this.ydoc = new Y.Doc();
		this.yEntities = this.ydoc.getArray("entities");
		this.yPlayers = this.ydoc.getMap("players");

		// Cache DOM elements
		this.logEl = content.querySelector("#log");
		this.sceneEl = content.querySelector("#scene-content");
		this.playersEl = content.querySelector("#players");
		this.rigEl = content.querySelector("#rig");

		this.setupContent(content);
		this.setupRTCListeners();
		this.setupPositionReader();
		this.setupObservers();
		this.initInstructions();

		/* load or not the initial state? */
		this.loadState();
	}

	setupContent(content) {
		const initButton = content.querySelector('#initScene');
		if (initButton) {
			initButton.addEventListener("click", () => this.initScene());
		}

		const loadStateButton = content.querySelector('#loadState');
		if (loadStateButton) {
			loadStateButton.addEventListener("click", () => this.loadState());
		}

		const saveStateButton = content.querySelector('#saveState');
		if (saveStateButton) {
			saveStateButton.addEventListener("click", () => this.saveState());
		}

		content.querySelectorAll("button[data-type]").forEach((button) => {
			button.addEventListener("click", () => {
				this.createEntityAtCamera(button.dataset.type);
			});
		});

		content.querySelector('matrix-room').setAttribute("profile-id", this.config.roomId)

		this.appendChild(content);
	}

	setupRTCListeners() {
		this.rtcUser.addEventListener("dataChannel", () => {
			this.logMessage("Connected to peer");
		});

		this.rtcUser.addEventListener("channelMessage", (event) => {
			const reader = new FileReader();
			reader.onload = () => {
				Y.applyUpdate(this.ydoc, new Uint8Array(reader.result));
			};
			reader.readAsArrayBuffer(event.detail);
		});

		this.rtcUser.addEventListener("disconnect", () => {
			this.logMessage("Disconnected from peer");
		});
	}

	setupPositionReader() {
		const yPlayers = this.yPlayers;
		const userId = this.userId
		AFRAME.registerComponent("position-reader", {
			init: function() {
				this.updatePosition = () => {
					const position = this.el.object3D.position;
					const rotation = this.el.object3D.rotation;
					const playerData = {
						position: `${position.x} ${position.y} ${position.z}`,
						rotation: `${rotation.x} ${rotation.y} ${rotation.z}`,
						color: this.userColor,
					};
					yPlayers.set(userId, playerData);
				};
			},
			tick: function() {
				this.updatePosition();
			},
		});
	}

	setupObservers() {
		this.yEntities.observe(() => this.renderScene());
		this.yPlayers.observe(() => this.renderPlayers());
		this.yEntities.observe(() => this.sendUpdate());
		this.yPlayers.observe(() => this.sendUpdate());
	}

	async saveState() {
		try {
			const tempDoc = new Y.Doc();
			const tempEntities = tempDoc.getArray("entities");
			tempEntities.push([...this.yEntities]);

			const content = { state: Array.from(Y.encodeStateAsUpdate(tempDoc)) };
			const res = await mwc.api.sendStateEvent({
				room_id: this.config.roomId,
				event_type: this.config.eventType,
				state_key: this.config.stateKey,
				content,
			});
			this.logMessage(res.error ? `${res.errcode}: ${res.error}` : "YJS state (excluding players) saved to Matrix room.");
		} catch (error) {
			this.logMessage(`Failed to save state: ${error.message}`);
		}
	}

	async loadState() {
		try {
			const stateEvents = await mwc.api.getRoomStateEvents({
				roomId: this.config.roomId,
				eventType: this.config.eventType,
				stateKey: this.config.stateKey,
			}).then(events => events.filter(event => event.type === this.config.eventType));

			const stateEvent = stateEvents[0];
			if (stateEvent?.content?.state) {
				const content = new Uint8Array(stateEvent.content.state);
				const tempDoc = new Y.Doc();
				Y.applyUpdate(tempDoc, content);
				const tempEntities = tempDoc.getArray("entities");
				this.yEntities.push([...tempEntities]);
				this.logMessage("YJS state (excluding players) loaded from Matrix room.");
			} else {
				this.logMessage("No state found in Matrix room.");
			}
		} catch (error) {
			this.logMessage(`Failed to load state: ${error.message}`);
		}
	}

	createEntity(type, position) {
		const entity = {
			id: `${type}-${Date.now()}`,
			type: type,
			position: position,
			color: `#${Math.floor(Math.random() * 16777215).toString(16)}`,
		};
		if (type === "plane") {
			entity.rotation = "-90 0 0";
			entity.scale = "25 25 25";
			entity.material = "side: double";
		}
		return entity;
	}

	createEntityAtCamera(type) {
		const cameraPosition = this.rigEl.object3D.position;

		let position;
		if (type === "plane") {
			position = `${cameraPosition.x} ${cameraPosition.y - 1.6 + (Math.random() * 0.01)} ${cameraPosition.z}`;
		} else {
			position = `${cameraPosition.x} ${cameraPosition.y} ${cameraPosition.z}`;
		}

		const entity = this.createEntity(type, position);
		this.yEntities.push([entity]);
	}

	renderScene() {
		this.sceneEl.innerHTML = "";
		this.yEntities.forEach((entity) => {
			this.sceneEl.appendChild(this.renderEntity(entity));
		});
	}

	renderEntity(entity) {
		const el = document.createElement(`a-${entity.type}`);
		el.setAttribute("position", entity.position);
		el.setAttribute("rotation", entity.rotation);
		el.setAttribute("animation", entity.animation);
		el.setAttribute("scale", entity.scale);
		el.setAttribute("color", entity.color);
		el.id = entity.id;
		return el;
	}

	renderPlayers() {
		this.yPlayers.forEach((data, id) => {
			this.renderPlayer(id, data);
		});
	}

	renderPlayer(id, data) {
		let playerEl = document.getElementById(`player-${id}`);
		if (!playerEl) {
			playerEl = document.createElement("a-entity");
			playerEl.id = `player-${id}`;

			const avatar = document.createElement("a-box");
			avatar.setAttribute("width", "0.5");
			avatar.setAttribute("height", "1.6");
			avatar.setAttribute("depth", "0.5");
			avatar.setAttribute("color", data.color || "green");

			const label = document.createElement("a-text");
			label.setAttribute("value", id);
			label.setAttribute("position", "0 2 0");
			label.setAttribute("align", "center");
			label.setAttribute("side", "double");
			label.setAttribute("color", "black");

			playerEl.appendChild(avatar);
			playerEl.appendChild(label);
			this.playersEl.appendChild(playerEl);
		}

		playerEl.setAttribute("position", data.position);
		playerEl.setAttribute("rotation", data.rotation);
	}

	initScene() {
		this.yEntities.push([
			{
				type: "plane",
				position: "0 0 0",
				color: "lightgray",
				rotation: "-90 0 0",
				scale: "25 25 25",
			},
			{
				type: "sky",
				position: "0 0 0",
				color: "lightblue",
			},
			{
				type: "box",
				width: "0.5",
				height: "1.6",
				depth: "0.5",
				color: "purple",
				position: "0 0.5 0",
				animation: "property: rotation; to: 0 360 0; loop: true; dur: 10000"
			}
		]);
	}

	sendUpdate() {
		if (this.rtcUser?.dataChannel) {
			this.rtcUser.send(Y.encodeStateAsUpdate(this.ydoc));
		}
	}

	initInstructions() {
		this.logMessage("Example page (best used on desktop devices) to synchronize an aframe 3d scene as YJS document between WebRTC peers, and save the document to a matrix.org room state event.");
		this.logMessage("Use the mouse and the keybord keys `WASD` to move; `Ctrl + Alt + i` to open the a-frame editor.");
		this.logMessage("In the aframe editor, only the #rig position is bound to the YJS document (and the rtc data channel, and the matrix room state). From there the scene can be exported as gltf, or saved locally as HTML file.")
		this.logMessage("Start adding entities/elements to the scene (right at the player position), or load a scene from the matrix room, and synchronize a state between rtc-user");
		this.logMessage("To save the scene state in a matrix room, the user should be a registered matrix user (non guest), and allow to send state events in the room (admin, moderator etc.)");
		this.logMessage("As the entire YJS document is stored as one matrix state event, it has a limitation is size, and therefore so does the aframe world that can be saved there.");
		this.logMessage("(A next version should explore using each matrix room events to construct the 3d scene, maybe without YJS? matrix timeseries CRDT).");
		this.logMessage("Available URL hash params #roomId #name #color");
	}

	logMessage(message) {
		const line = document.createElement("div");
		line.textContent = `${new Date().toLocaleTimeString()}: ${message}`;
		this.logEl.appendChild(line);
		this.logEl.scrollTop = this.logEl.scrollHeight;
	}
}
