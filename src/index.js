import "@sctlib/qr-code-utils";

import RTCUser from "./rtc-user.js";
import RTCPeer from "./rtc-peer.js";
import RTCSignaling from "./rtc-signaling.js";
import RTCSignalingRead from "./rtc-signaling-read.js";
import RTCSignalingShare from "./rtc-signaling-share.js";
import RTCSignalingPaste from "./rtc-signaling-paste.js";
import RTCSignalingCopy from "./rtc-signaling-copy.js";
import RTCSignalingMatrixIn from "./rtc-signaling-matrix-in.js";
import RTCSignalingMatrixOut from "./rtc-signaling-matrix-out.js";

const componentDefinitions = {
	"rtc-user": RTCUser,
	"rtc-peer": RTCPeer,
	"rtc-signaling": RTCSignaling,
	"rtc-signaling-read": RTCSignalingRead,
	"rtc-signaling-share": RTCSignalingShare,
	"rtc-signaling-paste": RTCSignalingPaste,
	"rtc-signaling-copy": RTCSignalingCopy,
	"rtc-signaling-matrix-in": RTCSignalingMatrixIn,
	"rtc-signaling-matrix-out": RTCSignalingMatrixOut,
};

export function defineComponents(components = componentDefinitions) {
	Object.entries(components).map(([cTag, cDef]) => {
		if (!customElements.get(cTag)) {
			customElements.define(cTag, cDef);
		}
	});
}

defineComponents();

export default componentDefinitions;
