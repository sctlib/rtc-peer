import { LitElement, html } from "lit";
import { peerStyles } from "./styles/peer.js";

/* a rtc peer (data) connection */
export default class RTCPeer extends LitElement {
	static styles = [peerStyles];
	static properties = {
		iceConnectionState: { attribute: "ice-connection-state" },
		offerBundle: { attribute: "offer-bundle" },
		receiveChannel: { state: true },
	};
	pcConfig = {
		iceServers: [
			{
				urls: ["stun:stun.nextcloud.com:443", "stun:stun.l.google.com:19302"],
			},
		],
	};

	initLocalPeer() {
		try {
			this.pc = new RTCPeerConnection(this.pcConfig);
		} catch (e) {
			console.error("Error init peer", e);
			return;
		}

		/* add PC event listeners */
		this.pc.oniceconnectionstatechange =
			this._onIceConnectionStateChange.bind(this);
		this.pc.onicegatheringstatechange =
			this._onIceGatheringStateChange.bind(this);
		this.pc.ondatachannel = this.onDataChannel.bind(this);
		this.pc.onnegotiationneeded = this.onNegotiationNeeded.bind(this);
		this.pc.onicecandidateerror = (event) =>
			console.error("pcicecandidateerror", event);
		this.pc.onicecandidate = this._onIceCandidate.bind(this);
		this.sendChannel = this.pc.createDataChannel("main");
		return this.pc;
	}
	_onIceCandidate(event) {
		/*
			 https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/iceconnectionstatechange_event#state_transitions_as_negotiation_ends
			 https://bugzilla.mozilla.org/show_bug.cgi?id=1540614
		 */
		/* console.info("onicecandidate", event); */
	}
	async _onIceConnectionStateChange(event) {
		if (
			this.pc.iceConnectionState === "disconnected" ||
			this.pc.iceConnectionState === "failed"
		) {
			this.pc.restartIce();
			try {
				const offer = await this.createOffer();
				await this.setLocalOffer(offer);
			} catch (e) {
				console.error("Error in connection restart:", e);
			}
		}
	}

	_onIceGatheringStateChange() {}

	onNegotiationNeeded(event) {
		const negotiationNeededEvent = new CustomEvent("negotiationNeeded", {
			bubbles: false,
			detail: event,
		});
		this.dispatchEvent(negotiationNeededEvent);
	}

	/*
		 api to manipulate the Peer Connection
	 */
	async createOffer() {
		/* https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API/Session_lifetime#ice_restart */
		return await this.pc.createOffer({ iceRestart: true });
	}
	async setRemoteOffer(offer) {
		if (!this.pc.localDescription) {
			await this.pc.setRemoteDescription(new RTCSessionDescription(offer));
		}
	}
	async setLocalOffer(offer) {
		try {
			await this.pc.setLocalDescription(offer);
		} catch (e) {
			console.error("Error setting local description", e);
		}
	}

	async createAnswer() {
		if (this.pc.remoteDescription && !this.pc.localDescription) {
			this.answer = await this.pc.createAnswer();
		}
		return this.answer;
	}
	async setRemoteAnswer(answer) {
		if (!this.pc.remoteDescription) {
			await this.pc.setRemoteDescription(answer);
		}
	}
	async setLocalAnswer(answer) {
		if (!this.pc.currentRemoteDescription) {
			await this.pc.setLocalDescription(answer);
		}
	}
	async setRemoteCandidate(candidate) {
		this.pc.addIceCandidate(candidate);
	}
	async gatherIceCandidates() {
		const localCandidates = [];
		return new Promise((resolve) => {
			this.pc.onicecandidate = ({ target, candidate }) => {
				if (target.iceGatheringState === "complete") {
					return resolve(localCandidates);
				} else {
					if (candidate) {
						localCandidates.push(candidate.toJSON());
					}
				}
			};
		});
	}
	onDataChannel({ channel }) {
		this.receiveChannel = channel;
		channel.onopen = this.onReceiveChannel.bind(this);
		channel.onmessage = this.onMessage.bind(this);
	}
	onReceiveChannel(event) {
		const dataChannelOpen = new CustomEvent("receiveChannel", {
			bubbles: false,
			composed: true,
			detail: event,
		});
		this.dispatchEvent(dataChannelOpen);
	}
	onMessage({ data }) {
		if (data) {
			const messageEvent = new CustomEvent("message", {
				bubbles: false,
				composed: true,
				detail: data,
			});
			this.dispatchEvent(messageEvent);
		}
	}
	/* send binary data */
	send(data) {
		this.sendChannel.send(data);
	}
	stopConnection() {
		this.pc.close();
	}
}
