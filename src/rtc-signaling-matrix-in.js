import { LitElement, html } from "lit";
import { MATRIX_EVENT_TYPE } from "./utils.js";
import { matrixAuthStyles } from "./styles/mre.js";
import { formStyles, inputStyles } from "./styles/form.js";
import mwc from "@sctlib/mwc";
const { api: matrixApi } = mwc;

export default class RTCSignalingMatrixIn extends LitElement {
	static properties = {
		allowedUsers: {
			type: Array,
			reflect: true,
			attribute: "allowed-users",
		},
	};
	get syncParams() {
		const filter = {
			room: {
				not_rooms: ["*"], // Exclude all rooms
			},
			account_data: {
				types: ["to_device"],
				/* not_types: ["*"], // Exclude all account_data types */
			},
			presence: {
				not_types: ["*"], // Exclude all presence event types
			},
		};
		const params = [["filter", filter]];
		return params;
	}

	constructor() {
		super();
		this.allowedUsers = [];
	}
	async connectedCallback() {
		super.connectedCallback();
		this.synchronise();
	}
	disconnectedCallback() {
		try {
			this.syncAbortController.abort();
		} catch (e) {
			/* console.log(e); */
		}
	}
	async synchronise(previousSync = {}) {
		if (previousSync.errcode) return;
		this.syncAbortController = new AbortController();
		let params = [this.syncParams];
		if (previousSync && previousSync.next_batch) {
			params.push(["since", previousSync.next_batch]);
		}
		let sync;
		try {
			sync = await matrixApi.sync({
				signal: this.syncAbortController.signal,
				params,
			});
			if (!sync.next_batch) {
				throw sync;
			}
		} catch (error) {
			console.info("Error syncing matrix events", error);
			return;
		}
		this.processSyncResponse(sync);
		await this.synchronise(sync);
	}

	processSyncResponse(sync) {
		const events = sync.to_device ? sync.to_device.events : [];
		if (events.length > 0) {
			this.handleSendToDeviceMessages(events);
		}
		console.info("Sync matrix", sync);
	}

	handleSendToDeviceMessages(events) {
		events.forEach((event) => {
			const { sender, type, content } = event;
			if (sender && content && type === MATRIX_EVENT_TYPE) {
				if (
					this.allowedUsers.includes(sender) ||
					this.allowedUsers[0] === "*"
				) {
					const matrixInEvent = new CustomEvent("input", {
						bubbles: true,
						detail: event,
					});
					this.dispatchEvent(matrixInEvent);
				} else {
					console.info("Dropped matrix peer offer from %s", sender, event);
				}
			}
		});
	}

	_onSubmit(event) {
		event.stopPropagation();
		event.preventDefault();
	}
	_onInput(event) {
		const { name, value } = event.target;
		event.stopPropagation();
		event.preventDefault();
		if (name === "allowedUsers") {
			this[name] = value.split(",").map((id) => id.trim());
		}
	}

	render() {
		return html`
			<form @submit=${this._onSubmit}>
				<label for="allowedUsers">
					user_id(s) allowed (default: all-rejected)
				</label>
				<input required
							 id="allowedUsers"
							 type="search"
							 name="allowedUsers"
							 part="input"
							 placeholder="@user-a:domain.tld,@user-b:domain.tld || *"
							 title="Enter Matrix.org user_id(s), to accept send-to-device request they might send you"
					.value=${this.allowedUsers.join(",")}
					@input=${this._onInput}
					></input >
			</form>
		`;
	}
	static styles = [matrixAuthStyles, formStyles, inputStyles];
}
