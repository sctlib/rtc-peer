import { LitElement, html } from "lit";
import { formStyles, inputStyles, buttonStyles } from "./styles/form.js";

export default class RTCSignalingMatrixOut extends LitElement {
	static properties = {
		usernames: { type: Array },
		userPeer: { type: Object },
		signalSent: { type: Boolean, state: true },
	};
	constructor() {
		super();
		this.usernames = [];
		this.userPeer = {};
		this.signalSent = false;
	}
	connectedCallback() {
		super.connectedCallback();
		if (this.userPeer.matrix_user_id) {
			this.usernames = [this.userPeer.matrix_user_id];
		}
	}
	render() {
		if (this.signalSent) {
			return this._renderReSendSignal();
		} else {
			return this._renderSendSignal();
		}
	}
	_renderSendSignal() {
		return html`
			<matrix-auth title="Log-in a matrix user account, to send the RTC offer to an other matrix user devices, or use the default guest user to be reached">
				<div slot="logged-out"></div>
				<div slot="logged-in"></div>
			</matrix-auth>
			<form @submit=${this._onSubmit}>
				<fieldset>
					<label for="usernames">
						to matrix user_id(s):
					</label>
					<input required
								 id="usernames"
								 type="username"
								 name="usernames"
								 part="input"
								 placeholder="@username:server.tld"
								 value=${this.usernames.map((id) => id.trim()).join(",")}
						@input=${this._onInput}
								 title="Enter a Matrix.org username(s), for a send-to-device request to all this user's devices"
						></input >
				</fieldset>
				<fieldset>
					<button type="submit" part="submit">send</button>
				</fieldset>
			</form>
		`;
	}
	_renderReSendSignal() {
		return html` <button type="button" @click=${this._onReSubmit}>
			re-send?
		</button>`;
	}
	_onInput = (event) => {
		const { target } = event;
		event.stopPropagation();
		event.preventDefault();
		if (target.name === "usernames") {
			this[target.name] = target.value.split(",").map((id) => id.trim());
		} else {
			this[target.name] = [target.value];
		}
	};
	_onSubmit = (event) => {
		event.stopPropagation();
		event.preventDefault();
		const pasteEvent = new CustomEvent("submit", {
			bubbles: true,
			detail: this.usernames,
		});
		this.dispatchEvent(pasteEvent);
		this.signalSent = true;
	};
	_onReSubmit() {
		this.signalSent = false;
	}
	static styles = [formStyles, inputStyles, buttonStyles];
}
