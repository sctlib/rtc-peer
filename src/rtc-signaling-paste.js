import { LitElement, html } from "lit";
import { inputStyles } from "./styles/form.js";

export default class RTCSignalingPaste extends LitElement {
	static properties = {
		data: { type: String, state: true },
	};
	constructor() {
		super();
		this.data = "";
	}
	render() {
		return html`
			<input
				required
				type="url"
				name="peer_data"
				part="input"
				placeholder=${"↷ Paste peer data URL"}
				@paste=${this.onPaste}
				@input=${(e) => {
					e.target.value = "";
				}}
				title=${`Paste in the URL of an offer or answer sent by a peer you are trying to connect with. {offer,answer,candidates,*bundle} ex: ${window.location.origin}?data=abc123`}
			></input >
		`;
	}
	onPaste = (event) => {
		event.stopPropagation();
		event.preventDefault();

		let paste = (event.clipboardData || window.clipboardData).getData("text");

		if (!paste) return;
		const pasteEvent = new CustomEvent("paste", {
			bubbles: true,
			detail: paste,
		});
		this.dispatchEvent(pasteEvent);
		return paste;
	};
	static styles = [inputStyles];
}
