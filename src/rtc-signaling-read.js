import { signalingReadStyles } from "./styles/signaling-read.js";
import { decodeShareUrl, MATRIX_EVENT_TYPE } from "./utils.js";
import { LitElement, html } from "lit";
import { createRef, ref } from "lit/directives/ref.js";

export default class RTCSignalingRead extends LitElement {
	scannerRef = createRef();

	static properties = {
		gatheredPeerBundle: { type: Object, state: true },
		method: { type: String },
		matrixPeers: { type: Array },
	};
	constructor() {
		super();
		this.gatheredPeerBundle = {};
		this.method = "";
		this.matrixPeers = [];
	}

	render() {
		switch (this.method) {
			case "copypaste":
				return this._renderPaste();
			case "qr-code":
				return this._renderQRScanner();
			case "qr-codes":
				return this._renderQRScanner();
			case "qr-codes-auto":
				return this._renderQRScanner({ auto: true, interval: 250 });
			case "matrix-user-device":
				return this._renderMatrixDevice();
			default:
				return html``;
		}
	}
	_renderQRScanner({ auto = false, interval } = {}) {
		interval = interval > 111 ? interval : 500;
		return html`
			<qr-code-scanner
				@success=${this.onQrRead}
				text-start="▒"
				text-stop="◍"
				?open=${auto}
				interval=${interval}
				${ref(this.scannerRef)}
			></qr-code-scanner>
		`;
	}
	_renderPaste() {
		return html`
			<rtc-signaling-paste @paste=${this._onPaste}></rtc-signaling-paste>
		`;
	}
	_renderMatrixDevice() {
		return html`
			<rtc-signaling-matrix-in
				@input=${this._onMatrix}
				.allowedUsers=${this.matrixPeers}
			></rtc-signaling-matrix-in>
		`;
	}

	/* a public method, to signal that a peer has been "manually found" */
	onPeer(data) {
		const peerEvent = new CustomEvent("peer", {
			bubbles: true,
			detail: data,
		});
		this.dispatchEvent(peerEvent);
	}

	/* pasting only handle a full peer bundle for now */
	_onPaste({ detail }) {
		const peerData = decodeShareUrl(detail);
		if (peerData) this.onPeer(peerData);
	}
	_onMatrix({ detail }) {
		const { content, sender, type } = detail;
		const { rtc_data_url } = content;
		if (rtc_data_url && sender && type === MATRIX_EVENT_TYPE) {
			let peerData = decodeShareUrl(rtc_data_url);
			peerData.matrix_user_id = sender;
			this.onPeer(peerData);
		}
	}

	/* QR scanner found barcodes in one scan,
		 decode its data and handle it if it is peer data */
	async onQrRead({ detail: barcodes }) {
		barcodes.forEach((barcode) => {
			let peerData = decodeShareUrl(barcode.rawValue);
			if (!peerData) return;
			this._scanFeedback();
			const fullPeer = this._readPartialPeer(peerData);
			if (fullPeer) {
				this.onPeer(fullPeer);
				this.scannerRef.value.stop();
			}
		});
	}

	/* read peer data, and use this.gatheredPeerBundle,
		 to store a partial peer data (from reading one QR in the list of QRs)
		 When we have a "full" peer bundle, we're done, return it */
	_readPartialPeer(data) {
		if (typeof data !== "object") return;

		// if there is already a full bundle of peer data, it is the full peer
		if (
			["offerbundle", "answerbundle"].indexOf(data.type) > -1 &&
			data.candidates
		) {
			return data;
		}

		// gather a peer bundle until it is fully gathered
		const doneGathering = this._bundleDoneGathering();
		if (!doneGathering) {
			this._addPeerDataToBundle(data);
		} else {
			return this.gatheredPeerBundle;
		}
	}

	/* is the bundle complete (offer||answer, type, candidates[candidate...]) */
	_bundleDoneGathering() {
		const { type, candidates, offer, answer } = this.gatheredPeerBundle;
		if (!type) return;
		if (!offer && !answer) return;
		if (!candidates || !candidates.length) return;
		if (!this._endOfCandidatesSignal(candidates)) return;
		return true;
	}

	/* do we have all candidates in the bundle.candidates?
		 https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/iceconnectionstatechange_event#state_transitions_as_negotiation_ends
	 */
	_endOfCandidatesSignal(candidates) {
		return candidates.find((c) => c.candidate === "");
	}

	/* Read peer data, let's add it to the bundle until we have all data;
		 the peerData is an object, with one direct children (object);
		 The key of the children, is the name of the "peerData",
		 except for `candidate`, only one candidate,
		 (for `candidates`, it is the full list of `candidate`)
		 */
	_addPeerDataToBundle(peerData) {
		const peerDataKey = Object.keys(peerData)[0];
		if (peerData.candidate || peerData.candidate === "") {
			if (!this.gatheredPeerBundle.candidates) {
				this.gatheredPeerBundle.candidates = [];
			} else if (!this._candidateInBundle(peerData, this.gatheredPeerBundle)) {
				this.gatheredPeerBundle.candidates = [
					...this.gatheredPeerBundle.candidates,
					peerData,
				];
			}
		} else if (peerData[peerDataKey]) {
			this.gatheredPeerBundle[peerDataKey] = peerData[peerDataKey];
		}
	}

	/* have we already received this candidate */
	_candidateInBundle(candidatePeer, { candidates }) {
		return candidates.find((c) => {
			return candidatePeer.candidate === c.candidate;
		});
	}

	/* give a visual feedback on the video, when scan peer data success */
	_scanFeedback() {
		const $scanner = this.scannerRef.value;
		$scanner.setAttribute("success", true);
		window.setTimeout(() => {
			$scanner && $scanner.removeAttribute("success");
		}, 200);
	}

	static styles = [signalingReadStyles];
}
