import { userStyles } from "./styles/user.js";
import { matrixAuthStyles } from "./styles/mre.js";
import { inputStyles, buttonStyles } from "./styles/form.js";
import { LitElement, html, css } from "lit";
import { createRef, ref } from "lit/directives/ref.js";
import { decode, SIGNALING_METHODS } from "./utils.js";
import "@sctlib/mwc";

/* a rtc user, that can create and connect to peers */
class RTCUser extends LitElement {
	peerRef = createRef();
	signalRef = createRef();
	static styles = [userStyles, matrixAuthStyles, inputStyles, buttonStyles];
	static properties = {
		signalingMethods: {
			type: Array,
			attribute: "signaling-methods",
			reflect: true,
		},
		signalingMethod: {
			type: String,
			attribute: "signaling-method",
			reflect: true,
		},
		data: {
			type: Object,
			state: true,
		},
		dataChannel: {
			type: Boolean,
			attribute: "data-channel",
			reflect: true,
		},
		userName: {
			type: String,
			reflect: true,
			attribute: "user-name",
		},
		userPeerName: {
			type: String,
			reflect: true,
			attribute: "user-peer-name",
		},
		searchParams: {
			type: Array,
			attribute: "search-params",
		},
		matrixPeers: {
			type: Array,
			reflect: true,
			attribute: "matrix-peers",
		},
		user: {
			type: Object,
			state: true,
			reflect: true,
		},
		userPeer: {
			type: Object,
			state: true,
			reflect: true,
			attribute: "user-peer",
		},
	};
	constructor() {
		super();
		this.user = {};
		this.userPeer = {};
		this.dataChannel = null;
		this.signalingMethods = [];
		this.userName = "";
		this.userPeerName = "";
		this.matrixPeers = [];
		this.searchParams = [];

		window.addEventListener("message", this._onWindowMessage.bind(this));
	}
	connectedCallback() {
		super.connectedCallback();
		if (!this.signalingMethods?.length) {
			this.signalingMethods = SIGNALING_METHODS;
		}
		if (this.userName) {
			this.setUser({ name: this.userName });
		}
		if (this.userPeerName) {
			this.setPeerUser({ name: this.userPeerName });
		}

		/* assign first "signaling-contact", then peer-data */
		const { matrixPeers, peerData, signalingMethods } = this._getSearchParams();
		if (
			this._shouldSearchParam("matrix-peers") &&
			matrixPeers &&
			matrixPeers.length
		) {
			this.matrixPeers = matrixPeers;
		}
		if (this._shouldSearchParam("peer-data") && peerData) {
			this.data = decode(peerData);
			window.history.pushState(
				window.document.title,
				window.document.title,
				window.location.origin,
			);
		} else {
			this.data = null;
		}

		if (this._shouldSearchParam("signaling-methods") && signalingMethods) {
			this.signalingMethods = signalingMethods;
		}
	}
	willUpdate(changedProperties) {
		if (changedProperties.has("user-name")) {
			this.setUser({ name: this.userName });
		}
		if (changedProperties.has("user-peer-name")) {
			this.setPeerUser({ name: this.userPeerName });
		}
	}

	_onWindowMessage(event) {
		/* maybe use
		https://developer.mozilla.org/en-US/docs/Web/API/Broadcast_Channel_API */
		/* console.log("window message", event); */
	}
	_getSearchParams() {
		const { hash, search } = window.location;
		const params = new URLSearchParams(hash ? hash.slice(1) : search);
		if (!params.size) return {};
		const matrixPeers = params.has("matrix-peers")
			? params
					.get("matrix-peers")
					.split(",")
					.map((id) => id.trim())
			: null;
		const signalingMethods = params.has("signaling-methods")
			? params.get("signaling-methods").split(",")
			: null;
		const peerData = params.has("peer-data") ? params.get("peer-data") : null;
		return { peerData, matrixPeers, signalingMethods };
	}
	_shouldSearchParam(param) {
		if (!this.searchParams || !this.searchParams.length) {
			return false;
		}
		return this.searchParams.includes(param);
	}

	render() {
		return html`
			${this._renderStatus()}
			${!this.dataChannel ? this._renderSignaling() : ""}
			<rtc-peer
				${ref(this.peerRef)}
				@signal=${this._onPeerSignal}
				@receiveChannel=${this._onReceiveChannel}
				@message=${this._onMessage}
			></rtc-peer>
			${this.dataChannel && this._renderChannelSlots()}
		`;
	}
	_renderChannelSlots() {
		return html`
			<slot name="send"></slot>
			<slot name="logs"></slot>
		`;
	}
	_renderSignaling() {
		return html` <rtc-signaling
			${ref(this.signalRef)}
			@signalOut=${this._onSignalOut}
			@signalIn=${this._onSignalIn}
			@offer=${this._onOfferSignal}
			.userPeer=${this.userPeer}
			.methods=${this.signalingMethods}
			.matrixPeers=${this.matrixPeers}
		></rtc-signaling>`;
	}
	_renderStatus() {
		const statuses = [
			this._renderUserStatus(),
			this._renderUserPeerStatus(),
			this._renderMatrixStatus(),
		];
		const { name } = this.user;
		return html`
			<details>
				<summary>${name ? name : "User"}</summary>
				<ul>
					${html`${statuses}`}
				</ul>
			</details>
		`;
	}
	_renderMatrixStatus() {
		if (this.signalingMethods.includes("matrix-user-device")) {
			return html`
				<li>
					<matrix-auth show-user="true" show-guest="true">
						<div slot="logged-out">
							<matrix-login></matrix-login>
						</div>
						<div slot="logged-in">
							<matrix-logout></matrix-logout>
						</div>
					</matrix-auth>
				</li>
			`;
		}
	}
	_renderUserStatus() {
		const _renderUserInfo = ([key, value]) => {
			return html`<li><strong>${key}</strong>: ${value}</li>`;
		};
		if (this.user) {
			return html`${Object.entries(this.user).map(_renderUserInfo)}`;
		}
	}
	_renderUserPeerStatus() {
		const _renderPeerInfo = ([key, value]) => {
			return html`<li><strong>${key}</strong>: ${value}</li>`;
		};
		if (this.userPeer) {
			/* rtc:// */
			return html`${Object.entries(this.userPeer).map(_renderPeerInfo)}`;
		}
	}

	_onSignalOut(signal) {
		/* console.log("signal out", signal); */
	}

	_onSignalIn({ detail: signal }) {
		this.readPeer(signal);
	}
	_onOfferSignal() {
		this.createPeerWithOffer();
	}
	_onPeerSignal({ detail }) {
		this.signalRef.value.signal(detail);
	}
	_onReceiveChannel({ detail }) {
		this.dataChannel = true;
		const dataChannelEvent = new CustomEvent("dataChannel", {
			bubbles: false,
			composed: true,
			detail,
		});
		this.dispatchEvent(dataChannelEvent);
		this.requestUpdate();
	}
	_onMessage({ detail: message }) {
		const messageEvent = new CustomEvent("channelMessage", {
			bubbles: false,
			composed: true,
			detail: message,
		});
		this.dispatchEvent(messageEvent);
	}
	_onSend({ detail }) {
		this.send(detail);
	}

	/* read an incoming peer from the outside signaling */
	async readPeer(peer = {}) {
		if (!peer) return;
		const handlePeerType = {
			offer: async () => this.createPeerFromOffer(peer),
			answer: async () => this.addAnswerToPeer(peer),
			candidate: async () => this.addCandidateToPeer(peer),
			offerbundle: async () => {
				if (peer.matrix_user_id) {
					this.readPeerUser(peer);
				}
				await this.readPeer(peer.offer);
				if (peer.candidates) {
					await Promise.all(peer.candidates.map((c) => this.readPeer(c)));
				}
			},
			answerbundle: async () => {
				await this.readPeer(peer.answer);
				if (peer.candidates) {
					await Promise.all(peer.candidates.map((c) => this.readPeer(c)));
				}
			},
		};

		/* is this ICE candidate or a "typed" peer */
		if (peer.candidate) {
			peer.type = "candidate";
		}
		if (peer.type) {
			await handlePeerType[peer.type]();
		}
	}

	readPeerUser({ matrix_user_id }) {
		if (matrix_user_id) {
			this.setPeerUser({ matrix_user_id });
		}
	}

	/* we offer signal, let's create a peer, and an offer */
	async createPeerWithOffer() {
		this.peerRef.value.initLocalPeer();
		const offer = await this.peerRef.value.createOffer();
		await this.peerRef.value.setLocalOffer(offer);
		const candidates = await this.peerRef.value.gatherIceCandidates();
		const bundle = { offer, candidates, type: "offerbundle" };
		this.signalRef.value.signalOut(bundle);
	}
	async createPeerFromOffer(offer) {
		this.peerRef.value.initLocalPeer();
		await this.peerRef.value.setRemoteOffer(offer);
		const answer = await this.peerRef.value.createAnswer();
		await this.peerRef.value.setLocalAnswer(answer);
		const candidates = await this.peerRef.value.gatherIceCandidates();
		const bundle = { answer, candidates, type: "answerbundle" };
		this.signalRef.value.signalOut(bundle);
	}
	async addAnswerToPeer(answer) {
		/* this.peerRef.value.initLocalPeer() */
		await this.peerRef.value.setRemoteAnswer(answer);
	}
	async addCandidateToPeer(candidate) {
		await this.peerRef.value.setRemoteCandidate(candidate);
	}

	/* when the data channel is open, send binary data;
		 https://developer.mozilla.org/en-US/docs/Web/API/RTCDataChannel/send */
	send(data) {
		this.peerRef.value.send(data);
	}

	/* set own "known user" (in/us) */
	setUser(user = {}) {
		if (Object.keys(user).length) {
			this.user = { ...this.user, ...user };
		}
	}
	/* set "known peer user" (out/they) (reachable via signaling methods) */
	setPeerUser(peerUser = {}) {
		if (Object.keys(peerUser).length) {
			this.userPeer = { ...this.userPeer, ...peerUser };
		}
	}
}

export default RTCUser;
