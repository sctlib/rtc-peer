import { css } from "lit";

const hostStyles = css`
	:host {
	}
	:host::part(menu) {
		padding: 0;
		margin: 0;
		list-style-position: inside;
	}
`;

const channelsStyles = css`
	:host::part(signals-out) {
		display: inline-flex;
		padding: 0;
	}
	:host::part(signal-out) {
		display: flex;
		align-items: center;
	}
`;

const helpText = css`
	:host::part(out)::before,
	:host::part(in)::before,
	:host::part(config)::before {
		display: inline-block;
		font-weight: bold;
		white-space: nowrap;
		padding: calc(var(--size) / 3);
	}
	:host::part(out)::before,
	:host::part(in)::before {
		content: "signals" "-" attr(part) "[" attr(length) "]";
	}
	:host::part(config)::before {
		content: "signaling-method";
	}
`;

const configStyles = css`
	1 :host::part(methods) {
		padding: calc(var(--size) / 3);
	}
`;

const createOfferStyles = css`
	:host::part(offer) {
		padding: calc(var(--size) / 3);
		cursor: pointer;
	}
`;

export const signalingStyles = css`
	${hostStyles}
	${configStyles}
	${channelsStyles}
	${createOfferStyles}
	${helpText}
`;
