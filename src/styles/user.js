import { css } from "lit";

/* position relative, for the qr-code scanning background? */
export const userStyles = css`
	:host {
		--size: 1.5rem;
		--c-border: slategray;
		--c-bg: var(--c-bg, whitesmoke);
		display: inline-block;
		padding: calc(var(--size) / 4);
		border: 1px solid var(--c-border);
		border-radius: calc(var(--size) / 8);
		background-color: var(--c-bg);
	}
	details {
		padding: calc(var(--size) / 3);
	}
	summary {
		cursor: pointer;
	}
`;
