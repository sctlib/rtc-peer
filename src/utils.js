import lz from "lz-string";

/*
	 native string compression is not yet supported on firefox,
	 https://stackoverflow.com/a/59469189
	 - so we use `lz-string` to compress & encode the sharable URLs
	 otherwise too long to share for manual message/qrcode signaling
	 - used to just encode with base64 (so JSON can go into the URL)
	 but QR codes were too big (and url on gitlab pages)
	 - could not benchmark `brocli` compression
 */

export const encode = (data) => {
	return lz.compressToEncodedURIComponent(JSON.stringify(data));
};

export const decode = (data) => {
	return JSON.parse(lz.decompressFromEncodedURIComponent(data));
};

export const buildShareUrl = (data) => {
	const encodedData = encode(data);
	const shareLocation = new URL(window.location);
	shareLocation.searchParams.set("peer-data", encodedData);
	const shareUrl = shareLocation.href;
	return shareUrl;
};

export const decodeShareUrl = (shareUrl) => {
	/* check if data in qr code is a url */
	let dataIsUrl;
	try {
		dataIsUrl = new URL(shareUrl);
	} catch (e) {
		console.info("The QR code data is not a URL;", shareUrl);
		return;
	}

	/* check if in the url there is a ?data= query param */
	if (!dataIsUrl.searchParams.get("peer-data")) {
		console.info("No ?data= query param in the QR-code URL");
		return;
	}

	let peerDecoded;
	try {
		const data = new URL(shareUrl).searchParams.get("peer-data");
		peerDecoded = decode(data);
	} catch (error) {
		console.info("No peer ?data= in URL could be parsed:", shareUrl, error);
		return;
	}
	return peerDecoded;
};

export const MATRIX_EVENT_TYPE = "io.gitlab.sctlib.rtc";
export const SIGNALING_METHODS = [
	"copypaste", // copy (out) and paste (in)
	"qr-codes-auto", // loop through peer data in multiple qr codes
	"qr-codes", // scroll to scan the peer data bits
	"qr-code", // one big qr hard to read on small screen or bad camera
	"matrix-user-device", // matrix-spec/#send-to-device-messaging
];
